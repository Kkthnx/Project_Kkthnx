local _, ns = ...
local PK = unpack(ns)
local Module = PK:RegisterModule("Unitframes")

function Module:OnLogin()
	if ProjectKkthnxUnitframeDB.classPortraits then
		Module.SetupClassPortaits()
	end

	if ProjectKkthnxUnitframeDB.classColorHealth then
		Module.SetupClassHealth()
	end
end
