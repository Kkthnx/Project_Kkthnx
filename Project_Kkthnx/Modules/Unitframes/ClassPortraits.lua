local _, ns = ...
local PK = unpack(ns)
local Module = PK:GetModule("Unitframes")

local _G = _G

local hooksecurefunc = _G.hooksecurefunc
local UnitIsPlayer = _G.UnitIsPlayer
local UnitClass = _G.UnitClass

local TEXTURE_NAME = "Interface\\AddOns\\Project_Kkthnx\\Media\\Textures\\ClassIcons\\%s.tga"

function Module:SetupClassPortaits()
	hooksecurefunc("UnitFramePortrait_Update", function(frame)
		if not frame then
			return
		end

		if frame.portrait and frame.unit then
			if UnitIsPlayer(frame.unit) then
				local _, class = UnitClass(frame.unit)
				frame.portrait:SetTexture(TEXTURE_NAME:format(class))
			end
		end
	end)
end