local _, ns = ...
local PK = unpack(ns)
local Module = PK:GetModule("Unitframes")

local _G = _G

local CUSTOM_CLASS_COLORS = _G.CUSTOM_CLASS_COLORS
local hooksecurefunc = _G.hooksecurefunc
local RAID_CLASS_COLORS = _G.RAID_CLASS_COLORS
local UnitClass = _G.UnitClass
local UnitIsConnected = _G.UnitIsConnected
local UnitIsPlayer = _G.UnitIsPlayer

function Module:SetupClassHealth()
	local function colorHealthBar(statusbar, unit)
		local _, class, color
		if UnitIsPlayer(unit) and UnitIsConnected(unit) and unit == statusbar.unit and UnitClass(unit) then
			_, class = UnitClass(unit)
			color = CUSTOM_CLASS_COLORS and CUSTOM_CLASS_COLORS[class] or RAID_CLASS_COLORS[class]
			statusbar:SetStatusBarColor(color.r, color.g, color.b)
		end
	end

	hooksecurefunc("UnitFrameHealthBar_Update", colorHealthBar)
	hooksecurefunc("HealthBar_OnValueChanged", function(self)
		colorHealthBar(self, self.unit)
	end)
end