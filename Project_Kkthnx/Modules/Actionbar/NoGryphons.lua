local _, ns = ...
local PK = unpack(ns)
local Module = PK:RegisterModule("NoGryphons")

local _G = _G

function Module:OnLogin()
	if not ProjectKkthnxActionbarDB.noGryphons then
		return
	end

	if _G.MainMenuBarArtFrame.LeftEndCap and _G.MainMenuBarArtFrame.RightEndCap then
		_G.MainMenuBarArtFrame.LeftEndCap:Hide()
		_G.MainMenuBarArtFrame.RightEndCap:Hide()
	end
end