local _, ns = ...
local PK = unpack(ns)
local Module = PK:RegisterModule("Tooltip")

function Module:OnLogin()
	if not ProjectKkthnxTooltipDB then -- Protect?
		return
	end

	if ProjectKkthnxTooltipDB.tooltipStyle then
		self:StyleTooltip()
	end

	if ProjectKkthnxTooltipDB.tooltipIcons then
		self:AddTooltipIcons()
	end

	if ProjectKkthnxTooltipDB.tooltipSpecItemLevel then
		self:AddSpecItemLevel()
	end
end