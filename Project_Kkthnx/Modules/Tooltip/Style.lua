local _, ns = ...
local PK, PKDB = unpack(ns)
local Module = PK:GetModule("Tooltip")
local L = ns.L

local reactionColor = {
	[1] = {0.85, 0.27, 0.27}, -- Hated
	[2] = {0.85, 0.27, 0.27}, -- Hostile
	[3] = {0.85, 0.27, 0.27}, -- Unfriendly
	[4] = {0.85, 0.77, 0.36}, -- Neutral
	[5] = {0.33, 0.59, 0.33}, -- Friendly
	[6] = {0.33, 0.59, 0.33}, -- Honored
	[7] = {0.33, 0.59, 0.33}, -- Revered
	[8] = {0.33, 0.59, 0.33}, -- Exalted
}

local StoryTooltip = QuestScrollFrame.StoryTooltip
StoryTooltip:SetFrameLevel(4)

local WarCampaignTooltip = QuestScrollFrame.WarCampaignTooltip

function Module:StyleTooltip()
	-- Hide PVP text
	PVP_ENABLED = ""

	-- Statusbar
	GameTooltipStatusBar:SetHeight(10)
	GameTooltipStatusBar:ClearAllPoints()
	GameTooltipStatusBar:SetPoint("BOTTOMLEFT", GameTooltip, "TOPLEFT", 3, 1)
	GameTooltipStatusBar:SetPoint("BOTTOMRIGHT", GameTooltip, "TOPRIGHT", -3, 1)
	GameTooltipStatusBar:SetBackdrop({bgFile = "Interface\\BUTTONS\\WHITE8X8",})
	GameTooltipStatusBar:SetBackdropColor(0.09, 0.09, 0.18, 0.99)

	-- Raid icon
	local ricon = GameTooltip:CreateTexture("GameTooltipRaidIcon", "OVERLAY")
	ricon:SetHeight(18)
	ricon:SetWidth(18)
	ricon:SetPoint("BOTTOM", GameTooltip, "TOP", 0, 5)

	GameTooltip:HookScript("OnHide", function(self) ricon:SetTexture(nil) end)

	-- Add "Targeted By" line
	local targetedList = {}
	local ClassColors = {}
	local token
	for class, color in next, RAID_CLASS_COLORS do
		ClassColors[class] = ("|cff%.2x%.2x%.2x"):format(color.r * 255, color.g * 255, color.b * 255)
	end

	local function AddTargetedBy()
		local numParty, numRaid = GetNumSubgroupMembers(), GetNumGroupMembers()
		if numParty > 0 or numRaid > 0 then
			for i = 1, (numRaid > 0 and numRaid or numParty) do
				local unit = (numRaid > 0 and "raid"..i or "party"..i)
				if UnitIsUnit(unit.."target", token) and not UnitIsUnit(unit, "player") then
					local _, class = UnitClass(unit)
					targetedList[#targetedList + 1] = ClassColors[class]
					targetedList[#targetedList + 1] = UnitName(unit)
					targetedList[#targetedList + 1] = "|r, "
				end
			end
			if #targetedList > 0 then
				targetedList[#targetedList] = nil
				GameTooltip:AddLine(" ", nil, nil, nil, 1)
				local line = _G["GameTooltipTextLeft"..GameTooltip:NumLines()]
				if not line then
					return
				end

				line:SetFormattedText(L["Targeted By"].." (|cffffffff%d|r): %s", (#targetedList + 1) / 3, table.concat(targetedList))
				wipe(targetedList)
			end
		end
	end

	-- Unit tooltip styling
	function GameTooltip_UnitColor(unit)
		if not unit then
			return
		end

		local r, g, b

		if UnitIsPlayer(unit) then
			local _, class = UnitClass(unit)
			local color = (CUSTOM_CLASS_COLORS or RAID_CLASS_COLORS)[class]
			if color then
				r, g, b = color.r, color.g, color.b
			else
				r, g, b = 1, 1, 1
			end
		elseif UnitIsTapDenied(unit) or UnitIsDead(unit) then
			r, g, b = 0.6, 0.6, 0.6
		else
			local reaction = reactionColor[UnitReaction(unit, "player")]
			if reaction then
				r, g, b = reaction[1], reaction[2], reaction[3]
			else
				r, g, b = 1, 1, 1
			end
		end

		return r, g, b
	end

	GameTooltipStatusBar:SetScript("OnValueChanged", function(self, value)
		if not value then
			return
		end

		local min, max = self:GetMinMaxValues()
		if (value < min) or (value > max) then
			return
		end

		self:SetStatusBarColor(0, 1, 0)
		local _, unit = GameTooltip:GetUnit()
		if unit then
			min, max = UnitHealth(unit), UnitHealthMax(unit)
			if not self.text then
				self.text = self:CreateFontString(nil, "OVERLAY", "Tooltip_Med")
				self.text:SetPoint("CENTER", GameTooltipStatusBar, 0, 1.5)
				self.text:SetShadowOffset(1, -1)
			end

			self.text:Show()
			local hp = PK.ShortValue(min).." / "..PK.ShortValue(max)
			self.text:SetText(hp)
		end
	end)

	local OnTooltipSetUnit = function(self)
		local lines = self:NumLines()
		local unit = (select(2, self:GetUnit())) or (GetMouseFocus() and GetMouseFocus().GetAttribute and GetMouseFocus():GetAttribute("unit")) or (UnitExists("mouseover") and "mouseover") or nil

		if not unit then
			return
		end

		local name, realm = UnitName(unit)
		local race, englishRace = UnitRace(unit)
		local level = UnitLevel(unit)
		local levelColor = GetCreatureDifficultyColor(level)
		local classification = UnitClassification(unit)
		local creatureType = UnitCreatureType(unit)
		local _, faction = UnitFactionGroup(unit)
		local _, playerFaction = UnitFactionGroup("player")
		local UnitPVPName = UnitPVPName

		if level and level == -1 then
			if classification == "worldboss" then
				level = "|cffff0000|r"..ENCOUNTER_JOURNAL_ENCOUNTER
			else
				level = "|cffff0000??|r"
			end
		end

		if classification == "rareelite" then
			classification = " R+"
		elseif classification == "rare" then
			classification = " R"
		elseif classification == "elite" then
			classification = "+"
		else
			classification = ""
		end


		_G["GameTooltipTextLeft1"]:SetText(name)

		if UnitIsPlayer(unit) then
			if UnitIsAFK(unit) then
				self:AppendText((" %s"):format("|cffE7E716"..L["[AFK]"].."|r"))
			elseif UnitIsDND(unit) then
				self:AppendText((" %s"):format("|cffFF0000"..L["[DND]"].."|r"))
			end

			if UnitIsPlayer(unit) and englishRace == "Pandaren" and faction ~= nil and faction ~= playerFaction then
				local hex = "cffff3333"
				if faction == "Alliance" then
					hex = "cff69ccf0"
				end
				self:AppendText((" [|%s%s|r]"):format(hex, faction:sub(1, 2)))
			end

			if GetGuildInfo(unit) then
				_G["GameTooltipTextLeft2"]:SetFormattedText("%s", GetGuildInfo(unit))
				if UnitIsInMyGuild(unit) then
					_G["GameTooltipTextLeft2"]:SetTextColor(1, 1, 0)
				else
					_G["GameTooltipTextLeft2"]:SetTextColor(0, 1, 1)
				end
			end

			local n = GetGuildInfo(unit) and 3 or 2
			-- thx TipTac for the fix above with color blind enabled
			if GetCVar("colorblindMode") == "1" then
				n = n + 1
			end
			_G["GameTooltipTextLeft"..n]:SetFormattedText("|cff%02x%02x%02x%s|r %s", levelColor.r * 255, levelColor.g * 255, levelColor.b * 255, level, race or UNKNOWN)

			for i = 2, lines do
				local line = _G["GameTooltipTextLeft"..i]
				if not line or not line:GetText() then return end
				if line and line:GetText() and (line:GetText() == FACTION_HORDE or line:GetText() == FACTION_ALLIANCE) then
					line:SetText()
					break
				end
			end
		else
			for i = 2, lines do
				local line = _G["GameTooltipTextLeft"..i]
				if not line or not line:GetText() or UnitIsBattlePetCompanion(unit) then return end
				if (level and line:GetText():find("^"..LEVEL)) or (creatureType and line:GetText():find("^"..creatureType)) then
					line:SetFormattedText("|cff%02x%02x%02x%s%s|r %s", levelColor.r * 255, levelColor.g * 255, levelColor.b * 255, level, classification, creatureType or "")
					break
				end
			end
		end

		if UnitExists(unit.."target") then
			local r, g, b = GameTooltip_UnitColor(unit.."target")
			local text = ""

			if UnitIsEnemy("player", unit.."target") then
				r, g, b = unpack(reactionColor[1])
			elseif not UnitIsFriend("player", unit.."target") then
				r, g, b = unpack(reactionColor[4])
			end

			if UnitName(unit.."target") == UnitName("player") then
				text = "|cfffed100"..STATUS_TEXT_TARGET..":|r ".."|cffff0000> "..UNIT_YOU.." <|r"
			else
				text = "|cfffed100"..STATUS_TEXT_TARGET..":|r "..UnitName(unit.."target")
			end

			self:AddLine(text, r, g, b)
		end

		local raidIndex = GetRaidTargetIndex(unit)
		if raidIndex then
			ricon:SetTexture("Interface\\TargetingFrame\\UI-RaidTargetingIcon_"..raidIndex)
		end

		token = unit AddTargetedBy()
	end

	GameTooltip:HookScript("OnTooltipSetUnit", OnTooltipSetUnit)

	-- Fix compare tooltips(by Blizzard)(../FrameXML/GameTooltip.lua)
	hooksecurefunc("GameTooltip_ShowCompareItem", function(self, anchorFrame)
		if not self then
			self = GameTooltip
		end

		if not anchorFrame then
			anchorFrame = self.overrideComparisonAnchorFrame or self
		end

		if self.needsReset then
			self:ResetSecondaryCompareItem()
			GameTooltip_AdvanceSecondaryCompareItem(self)
			self.needsReset = false
		end

		local shoppingTooltip1, shoppingTooltip2 = unpack(self.shoppingTooltips)
		local primaryItemShown, secondaryItemShown = shoppingTooltip1:SetCompareItem(shoppingTooltip2, self)
		local leftPos = anchorFrame:GetLeft()
		local rightPos = anchorFrame:GetRight()

		local side
		local anchorType = self:GetAnchorType()
		local totalWidth = 0
		if primaryItemShown then
			totalWidth = totalWidth + shoppingTooltip1:GetWidth()
		end
		if secondaryItemShown then
			totalWidth = totalWidth + shoppingTooltip2:GetWidth()
		end
		if self.overrideComparisonAnchorSide then
			side = self.overrideComparisonAnchorSide
		else
			-- Find correct side
			local rightDist = 0
			if not rightPos then
				rightPos = 0
			end
			if not leftPos then
				leftPos = 0
			end

			rightDist = GetScreenWidth() - rightPos

			if anchorType and totalWidth < leftPos and (anchorType == "ANCHOR_LEFT" or anchorType == "ANCHOR_TOPLEFT" or anchorType == "ANCHOR_BOTTOMLEFT") then
				side = "left"
			elseif anchorType and totalWidth < rightDist and (anchorType == "ANCHOR_RIGHT" or anchorType == "ANCHOR_TOPRIGHT" or anchorType == "ANCHOR_BOTTOMRIGHT") then
				side = "right"
			elseif rightDist < leftPos then
				side = "left"
			else
				side = "right"
			end
		end

		-- See if we should slide the tooltip
		if anchorType and anchorType ~= "ANCHOR_PRESERVE" then
			if (side == "left") and (totalWidth > leftPos) then
				self:SetAnchorType(anchorType, (totalWidth - leftPos), 0)
			elseif (side == "right") and (rightPos + totalWidth) > GetScreenWidth() then
				self:SetAnchorType(anchorType, -((rightPos + totalWidth) - GetScreenWidth()), 0)
			end
		end

		if secondaryItemShown then
			shoppingTooltip2:SetOwner(self, "ANCHOR_NONE")
			shoppingTooltip2:ClearAllPoints()
			shoppingTooltip1:SetOwner(self, "ANCHOR_NONE")
			shoppingTooltip1:ClearAllPoints()

			if side and side == "left" then
				shoppingTooltip1:SetPoint("TOPRIGHT", anchorFrame, "TOPLEFT", -3, -10)
			else
				shoppingTooltip2:SetPoint("TOPLEFT", anchorFrame, "TOPRIGHT", 3, -10)
			end

			if side and side == "left" then
				shoppingTooltip2:SetPoint("TOPRIGHT", shoppingTooltip1, "TOPLEFT", -3, 0)
			else
				shoppingTooltip1:SetPoint("TOPLEFT", shoppingTooltip2, "TOPRIGHT", 3, 0)
			end
		else
			shoppingTooltip1:SetOwner(self, "ANCHOR_NONE")
			shoppingTooltip1:ClearAllPoints()

			if side and side == "left" then
				shoppingTooltip1:SetPoint("TOPRIGHT", anchorFrame, "TOPLEFT", -3, -10)
			else
				shoppingTooltip1:SetPoint("TOPLEFT", anchorFrame, "TOPRIGHT", 3, -10)
			end

			shoppingTooltip2:Hide()
		end

		-- We have to call this again because :SetOwner clears the tooltip.
		shoppingTooltip1:SetCompareItem(shoppingTooltip2, self)
		shoppingTooltip1:Show()
	end)

	-- Fix GameTooltipMoneyFrame font size
	local function FixFont(self)
		for i = 1, 2 do
			if _G["GameTooltipMoneyFrame"..i] then
				_G["GameTooltipMoneyFrame"..i.."PrefixText"]:SetFontObject("GameTooltipText")
				_G["GameTooltipMoneyFrame"..i.."SuffixText"]:SetFontObject("GameTooltipText")
				_G["GameTooltipMoneyFrame"..i.."GoldButton"]:SetNormalFontObject("GameTooltipText")
				_G["GameTooltipMoneyFrame"..i.."SilverButton"]:SetNormalFontObject("GameTooltipText")
				_G["GameTooltipMoneyFrame"..i.."CopperButton"]:SetNormalFontObject("GameTooltipText")
			end
		end

		for i = 1, 2 do
			if _G["ItemRefTooltipMoneyFrame"..i] then
				_G["ItemRefTooltipMoneyFrame"..i.."PrefixText"]:SetFontObject("GameTooltipText")
				_G["ItemRefTooltipMoneyFrame"..i.."SuffixText"]:SetFontObject("GameTooltipText")
				_G["ItemRefTooltipMoneyFrame"..i.."GoldButton"]:SetNormalFontObject("GameTooltipText")
				_G["ItemRefTooltipMoneyFrame"..i.."SilverButton"]:SetNormalFontObject("GameTooltipText")
				_G["ItemRefTooltipMoneyFrame"..i.."CopperButton"]:SetNormalFontObject("GameTooltipText")
			end
		end
	end

	GameTooltip:HookScript("OnTooltipSetItem", FixFont)
	ItemRefTooltip:HookScript("OnTooltipSetItem", FixFont)
end