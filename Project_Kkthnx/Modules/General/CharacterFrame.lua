local _, ns = ...
local PK = unpack(ns)
local Module = PK:RegisterModule("CharacterFrame")

local _G = _G

local function UpdateAzeriteItem(self)
	if not self.styled then
		self.AzeriteTexture:SetAlpha(0)
		self.RankFrame.Texture:SetTexture()
		self.RankFrame.Label:SetFont(_G.STANDARD_TEXT_FONT, 13, "OUTLINE")
		self.RankFrame.Label:SetShadowOffset(0, 0)

		self.styled = true
	end

	self:GetHighlightTexture():SetColorTexture(1, 1, 1, .25)
	self:GetHighlightTexture():SetAllPoints()
end

local function UpdateAzeriteEmpoweredItem(self)
	self.AzeriteTexture:SetAtlas("AzeriteIconFrame")
	self.AzeriteTexture:SetPoint("TOPLEFT", self, "TOPLEFT", 0, 0)
	self.AzeriteTexture:SetPoint("BOTTOMRIGHT", self, "BOTTOMRIGHT", 0, 0)
	self.AzeriteTexture:SetTexCoord(0.08, 0.92, 0.08, 0.92)
	self.AzeriteTexture:SetDrawLayer("BORDER", 1)
end

function Module:OnLogin()
	if not ProjectKkthnxGeneralDB.characterFrame then
		return
	end

	if _G.CharacterFrame:IsShown() then
		_G.HideUIPanel(_G.CharacterFrame)
	end

	_G.CharacterModelFrame.BackgroundBotLeft:Kill()
	_G.CharacterModelFrame.BackgroundBotLeft:Kill()
	_G.CharacterModelFrame.BackgroundBotRight:Kill()
	_G.CharacterModelFrame.BackgroundOverlay:Kill()
	_G.CharacterModelFrame.BackgroundTopLeft:Kill()
	_G.CharacterModelFrame.BackgroundTopRight:Kill()
	_G.CharacterStatsPane.ClassBackground:Kill()
	_G.PaperDollInnerBorderBottom:Kill()
	_G.PaperDollInnerBorderBottom2:Kill()
	_G.PaperDollInnerBorderBottomLeft:Kill()
	_G.PaperDollInnerBorderBottomRight:Kill()
	_G.PaperDollInnerBorderLeft:Kill()
	_G.PaperDollInnerBorderRight:Kill()
	_G.PaperDollInnerBorderTop:Kill()
	_G.PaperDollInnerBorderTopLeft:Kill()
	_G.PaperDollInnerBorderTopRight:Kill()

	for _, slot in _G.pairs({_G.PaperDollItemsFrame:GetChildren()}) do
		if slot:IsObjectType("Button") or slot:IsObjectType("ItemButton") then
			slot:StripTextures()
			slot:SetSize(36, 36)

			_G.hooksecurefunc(slot, "DisplayAsAzeriteItem", UpdateAzeriteItem)
			_G.hooksecurefunc(slot, "DisplayAsAzeriteEmpoweredItem", UpdateAzeriteEmpoweredItem)

			if slot.popoutButton:GetPoint() == "TOP" then
				slot.popoutButton:SetPoint("TOP", slot, "BOTTOM", 0, 2)
			else
				slot.popoutButton:SetPoint("LEFT", slot, "RIGHT", -2, 0)
			end
		end
	end

	_G.CharacterHeadSlot:SetPoint("TOPLEFT", _G.CharacterFrame.Inset, "TOPLEFT", 6, -6)
	_G.CharacterHandsSlot:SetPoint("TOPRIGHT", _G.CharacterFrame.Inset, "TOPRIGHT", -6, -6)
	_G.CharacterMainHandSlot:SetPoint("BOTTOMLEFT", _G.CharacterFrame.Inset, "BOTTOMLEFT", 176, 5)
	_G.CharacterSecondaryHandSlot:ClearAllPoints()
	_G.CharacterSecondaryHandSlot:SetPoint("BOTTOMRIGHT", _G.CharacterFrame.Inset, "BOTTOMRIGHT", -176, 5)

	_G.CharacterModelFrame:SetSize(0, 0)
	_G.CharacterModelFrame:ClearAllPoints()
	_G.CharacterModelFrame:SetPoint("TOPLEFT", _G.CharacterFrame.Inset, 0, 0)
	_G.CharacterModelFrame:SetPoint("BOTTOMRIGHT", _G.CharacterFrame.Inset, 0, 30)
	_G.CharacterModelFrame:SetCamDistanceScale(1.1)

	_G.hooksecurefunc("CharacterFrame_Expand", function()
		_G.CharacterFrame:SetSize(640, 431) -- 540 + 100, 424 + 7
		_G.CharacterFrame.Inset:SetPoint("BOTTOMRIGHT", _G.CharacterFrame, "BOTTOMLEFT", 432, 4)

		local _, setClass = _G.UnitClass("player")
		if setClass then
			_G.CharacterFrame.Inset.Bg:SetTexture("Interface\\DressUpFrame\\DressingRoom" .. setClass)
			_G.CharacterFrame.Inset.Bg:SetTexCoord(1 / 512, 479 / 512, 46 / 512, 455 / 512)
			_G.CharacterFrame.Inset.Bg:SetHorizTile(false)
			_G.CharacterFrame.Inset.Bg:SetVertTile(false)
		end
	end)

	_G.hooksecurefunc("CharacterFrame_Collapse", function()
		_G.CharacterFrame:SetHeight(424)
		_G.CharacterFrame.Inset:SetPoint("BOTTOMRIGHT", _G.CharacterFrame, "BOTTOMLEFT", 332, 4)

		_G.CharacterFrame.Inset.Bg:SetTexture("Interface\\FrameGeneral\\UI-Background-Marble", "REPEAT", "REPEAT")
		_G.CharacterFrame.Inset.Bg:SetTexCoord(0, 1, 0, 1)
		_G.CharacterFrame.Inset.Bg:SetHorizTile(true)
		_G.CharacterFrame.Inset.Bg:SetVertTile(true)
	end)

	_G.CharacterLevelText:SetFont(_G.STANDARD_TEXT_FONT, 12, "")
	_G.CharacterStatsPane.ItemLevelFrame.Value:SetFont(_G.STANDARD_TEXT_FONT, 20, "OUTLINE")
end