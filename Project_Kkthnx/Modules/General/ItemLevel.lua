-- Display Equipment Level In Character Panel, Etc

local _, ns = ...
local PK, PKDB = unpack(ns)
local Module = PK:RegisterModule("ItemLevel")

local _G = _G

local SLOTIDS = {}
for _, slot in _G.pairs({"Head", "Neck", "Shoulder", "Shirt", "Chest", "Waist", "Legs", "Feet", "Wrist", "Hands", "Finger0", "Finger1", "Trinket0", "Trinket1", "Back", "MainHand", "SecondaryHand"}) do
	SLOTIDS[slot] = _G.GetInventorySlotInfo(slot.."Slot")
end

	local myString = _G.setmetatable({}, {
	__index = function(t, i)
		local gslot = _G["Character"..i.."Slot"]
		if not gslot then
			return
		end

		local fstr = gslot:CreateFontString(nil, "OVERLAY")
		fstr:SetFont(PKDB.Font[1], PKDB.Font[2] + 1, "OUTLINE")
		fstr:SetPoint("TOP", 0, -2)
		t[i] = fstr
		return fstr
	end
})

	local tarString = _G.setmetatable({}, {
	__index = function(t, i)
		local gslot = _G["Inspect"..i.."Slot"]
		if not gslot then
			return
		end

		local fstr = gslot:CreateFontString(nil, "OVERLAY")
		fstr:SetFont(PKDB.Font[1], PKDB.Font[2] + 1, "OUTLINE")
		fstr:SetPoint("TOP", 0, -2)

		t[i] = fstr
		return fstr
	end
})

function Module:ItemLevel_SetupLevel(unit, strType)
	if not _G.UnitExists(unit) then
		return
	end

	for slot, index in pairs(SLOTIDS) do
		local str = strType[slot]
		if not str then return end
		str:SetText("")

		local link = _G.GetInventoryItemLink(unit, index)
		if link and index ~= 4 then
			local _, _, quality, level = _G.GetItemInfo(link)
			level = PK.GetItemLevel(link, unit, index) or level

			if level and level > 1 and quality then
				local color = _G.BAG_ITEM_QUALITY_COLORS[quality]
				str:SetText(level)
				str:SetTextColor(color.r, color.g, color.b)
			end
		end
	end
end

function Module:ItemLevel_UpdateInspect(...)
	local guid = ...
	if _G.InspectFrame and _G.InspectFrame.unit and _G.UnitGUID(_G.InspectFrame.unit) == guid then
		Module:ItemLevel_SetupLevel(_G.InspectFrame.unit, tarString)
	end
end

-- iLvl on flyout buttons
function Module:ItemLevel_FlyoutUpdate(bag, slot, quality)
	if not self.iLvl then
		self.iLvl = self:CreateFontString(nil, "OVERLAY")
		self.iLvl:SetFont(PKDB.Font[1], PKDB.Font[2] + 1, "OUTLINE")
		self.iLvl:SetPoint("TOP", 0, -2)
	end

	local link, level
	if bag then
		link = _G.GetContainerItemLink(bag, slot)
		level = PK.GetItemLevel(link, bag, slot)
	else
		link = _G.GetInventoryItemLink("player", slot)
		level = PK.GetItemLevel(link, "player", slot)
	end

	local color = _G.BAG_ITEM_QUALITY_COLORS[quality or 1]
	self.iLvl:SetText(level)
	self.iLvl:SetTextColor(color.r, color.g, color.b)
end

function Module:ItemLevel_FlyoutSetup()
	local location = self.location
	if not location or location >= _G.EQUIPMENTFLYOUT_FIRST_SPECIAL_LOCATION then
		if self.iLvl then
			self.iLvl:SetText("")
		end
		return
	end

	local _, _, bags, voidStorage, slot, bag = _G.EquipmentManager_UnpackLocation(location)
	if voidStorage then return end
	local quality = _G.select(13, _G.EquipmentManager_GetItemInfoByLocation(location))
	if bags then
		Module.ItemLevel_FlyoutUpdate(self, bag, slot, quality)
	else
		Module.ItemLevel_FlyoutUpdate(self, nil, slot, quality)
	end
end

-- iLvl on scrapping machine
function Module:ItemLevel_ScrappingUpdate()
	if not self.iLvl then
		self.iLvl = self:CreateFontString(nil, "OVERLAY")
		self.iLvl:SetFont(PKDB.Font[1], PKDB.Font[2] + 1, "OUTLINE")
		self.iLvl:SetPoint("TOP", 0, -2)
	end

	if not self.itemLink then self.iLvl:SetText("")
		return
	end

	local quality = 1
	if self.itemLocation and not self.item:IsItemEmpty() and self.item:GetItemName() then
		quality = self.item:GetItemQuality()
	end

	local level = PK.GetItemLevel(self.itemLink)
	local color = _G.BAG_ITEM_QUALITY_COLORS[quality]
	self.iLvl:SetText(level)
	self.iLvl:SetTextColor(color.r, color.g, color.b)
end

function Module.ItemLevel_ScrappingShow(event, addon)
	if addon == "Blizzard_ScrappingMachineUI" then
		for button in pairs(ScrappingMachineFrame.ItemSlots.scrapButtons.activeObjects) do
			_G.hooksecurefunc(button, "RefreshIcon", Module.ItemLevel_ScrappingUpdate)
		end

		PK:UnregisterEvent(event, Module.ItemLevel_ScrappingShow)
	end
end

function Module:OnLogin()
	if not ProjectKkthnxGeneralDB.itemLevel then
		return
	end

	_G.hooksecurefunc("PaperDollItemSlotButton_OnShow", function()
		Module:ItemLevel_SetupLevel("player", myString)
	end)

	_G.hooksecurefunc("PaperDollItemSlotButton_OnEvent", function(self, event, id)
		if event == "PLAYER_EQUIPMENT_CHANGED" and self:GetID() == id then
			Module:ItemLevel_SetupLevel("player", myString)
		end
	end)

	PK:RegisterEvent("INSPECT_READY", self.ItemLevel_UpdateInspect)
	_G.hooksecurefunc("EquipmentFlyout_DisplayButton", self.ItemLevel_FlyoutSetup)
	PK:RegisterEvent("ADDON_LOADED", self.ItemLevel_ScrappingShow)

	MIN_PLAYER_LEVEL_FOR_ITEM_LEVEL_DISPLAY = 1
	hooksecurefunc("PaperDollFrame_SetItemLevel", function(self, unit)
		if unit ~= "player" then return end

		local total, equip = GetAverageItemLevel()
		if total > 0 then total = string.format("%.1f", total) end
		if equip > 0 then equip = string.format("%.1f", equip) end

		local ilvl = equip
		if equip ~= total then
			ilvl = equip.." / "..total
		end

		self.Value:SetText(ilvl)

		self.tooltip = "|cffffffff"..STAT_AVERAGE_ITEM_LEVEL..": "..ilvl
	end)
end