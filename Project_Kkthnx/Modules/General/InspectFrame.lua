local _, ns = ...
local PK = unpack(ns)
local Module = PK:RegisterModule("InspectFrame")

local _G = _G

local function SkinInspectFrame()
	if not ProjectKkthnxGeneralDB.inspectFrame then
		return
	end

	if _G.InspectFrame:IsShown() then
		_G.HideUIPanel(_G.InspectFrame)
	end

	_G.InspectModelFrameBackgroundBotLeft:Kill()
	_G.InspectModelFrameBackgroundBotRight:Kill()
	_G.InspectModelFrameBackgroundOverlay:Kill()
	_G.InspectModelFrameBackgroundTopLeft:Kill()
	_G.InspectModelFrameBackgroundTopRight:Kill()
	_G.InspectModelFrameBorderTopLeft:Kill()
	_G.InspectModelFrameBorderTopRight:Kill()
	_G.InspectModelFrameBorderTop:Kill()
	_G.InspectModelFrameBorderLeft:Kill()
	_G.InspectModelFrameBorderRight:Kill()
	_G.InspectModelFrameBorderBottomLeft:Kill()
	_G.InspectModelFrameBorderBottomRight:Kill()
	_G.InspectModelFrameBorderBottom:Kill()
	_G.InspectModelFrameBorderBottom2:Kill()

	for _, slot in _G.pairs({_G.InspectPaperDollItemsFrame:GetChildren()}) do
		if slot:IsObjectType("Button") or slot:IsObjectType("ItemButton") then
			slot:StripTextures()
			slot:SetSize(36, 36)
		end
	end

	_G.InspectHeadSlot:SetPoint("TOPLEFT",_G. InspectFrame.Inset, "TOPLEFT", 6, -6)
	_G.InspectHandsSlot:SetPoint("TOPRIGHT", _G.InspectFrame.Inset, "TOPRIGHT", -6, -6)
	_G.InspectMainHandSlot:SetPoint("BOTTOMLEFT", _G.InspectFrame.Inset, "BOTTOMLEFT", 176, 5)
	_G.InspectSecondaryHandSlot:ClearAllPoints()
	_G.InspectSecondaryHandSlot:SetPoint("BOTTOMRIGHT", _G.InspectFrame.Inset, "BOTTOMRIGHT", -176, 5)

	_G.InspectModelFrame:SetSize(0, 0)
	_G.InspectModelFrame:ClearAllPoints()
	_G.InspectModelFrame:SetPoint("TOPLEFT", _G.InspectFrame.Inset, 0, 0)
	_G.InspectModelFrame:SetPoint("BOTTOMRIGHT", _G.InspectFrame.Inset, 0, 30)
	_G.InspectModelFrame:SetCamDistanceScale(1.1)

	-- Adjust The Inset Based On Tabs
	local OnInspectSwitchTabs = function(newID)
		local tabID = newID or _G.PanelTemplates_GetSelectedTab(_G.InspectFrame)
		if tabID == 1 then
			_G.InspectFrame:SetSize(438, 431) -- 540 + 100, 424 + 7
			_G.InspectFrame.Inset:SetPoint("BOTTOMRIGHT", _G.InspectFrame, "BOTTOMLEFT", 432, 4)

			local _, targetClass = _G.UnitClass("target")
			if targetClass then
				_G.InspectFrame.Inset.Bg:SetTexture("Interface\\DressUpFrame\\DressingRoom"..targetClass)
				_G.InspectFrame.Inset.Bg:SetTexCoord(1 / 512, 479 / 512, 46 / 512, 455 / 512)
				_G.InspectFrame.Inset.Bg:SetHorizTile(false)
				_G.InspectFrame.Inset.Bg:SetVertTile(false)
			end
		else
			_G.InspectFrame:SetSize(338, 424)
			_G.InspectFrame.Inset:SetPoint("BOTTOMRIGHT", _G.InspectFrame, "BOTTOMLEFT", 332, 4)

			_G.InspectFrame.Inset.Bg:SetTexture("Interface\\FrameGeneral\\UI-Background-Marble", "REPEAT", "REPEAT")
			_G.InspectFrame.Inset.Bg:SetTexCoord(0, 1, 0, 1)
			_G.InspectFrame.Inset.Bg:SetHorizTile(true)
			_G.InspectFrame.Inset.Bg:SetVertTile(true)
		end
	end

	-- Hook it to tab switches
	_G.hooksecurefunc("InspectSwitchTabs", OnInspectSwitchTabs)
	-- Call it once to apply it from the start
	OnInspectSwitchTabs(1)
end

function Module:OnLogin()
	if _G.IsAddOnLoaded("Blizzard_InspectUI") then
		SkinInspectFrame()
	else
		local waitFrame = _G.CreateFrame("FRAME")
		waitFrame:RegisterEvent("ADDON_LOADED")
		waitFrame:SetScript("OnEvent", function(_, _, arg1)
			if arg1 == "Blizzard_InspectUI" then
				SkinInspectFrame()
				waitFrame:UnregisterAllEvents()
			end
		end)
	end
end