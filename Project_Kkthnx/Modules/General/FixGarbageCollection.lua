local _, ns = ...
local PK = unpack(ns)
local Module = PK:RegisterModule("FixGarbageCollection")

local _G = _G

local Module_FixGarbageCollection = _G.CreateFrame("Frame")

function Module:OnLogin()
	if not ProjectKkthnxGeneralDB.fixGarbageCollect then
		return
	end

	_G.blizzardCollectgarbage("setpause", 110)
	_G.blizzardCollectgarbage("setstepmul", 200)

	_G.collectgarbage = function(opt, arg)
		if (opt == "collect") or (opt == nil) then
		elseif (opt == "count") then
			return _G.blizzardCollectgarbage(opt, arg)
		elseif (opt == "setpause") then
			return _G.blizzardCollectgarbage("setpause", 110)
		elseif opt == "setstepmul" then
			return _G.blizzardCollectgarbage("setstepmul", 200)
		elseif (opt == "stop") then
		elseif (opt == "restart") then
		elseif (opt == "step") then
			if (arg ~= nil) then
				if (arg <= 10000) then
					return _G.blizzardCollectgarbage(opt, arg)
				end
			else
				return _G.blizzardCollectgarbage(opt, arg)
			end
		else
			return _G.blizzardCollectgarbage(opt, arg)
		end
	end

	-- Memory Usage Is Unrelated To Performance, And Tracking Memory Usage Does Not Track "BAD" Addons.
	-- Developers Can Uncomment This Line To Enable The Functionality When Looking For Memory Leaks,
	-- But For The Average End-user This Is A Completely Pointless Thing To Track.
	_G.UpdateAddOnMemoryUsage = function() end
end