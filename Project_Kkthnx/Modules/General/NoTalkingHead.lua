local _, ns = ...
local PK = unpack(ns)
local Module = PK:RegisterModule("NoTalkingHead")

local _G = _G

local function UpdateTalkingHead(self, event, ...)
	if (event == "ADDON_LOADED") then
		local addon = ...
		if (addon ~= "Blizzard_TalkingHeadUI") then
			return
		end
		PK:UnregisterEvent("ADDON_LOADED", UpdateTalkingHead)
	end

	if ProjectKkthnxGeneralDB.noTalkingHead then
		if _G.TalkingHeadFrame then
			_G.TalkingHeadFrame:UnregisterEvent("TALKINGHEAD_REQUESTED")
			_G.TalkingHeadFrame:UnregisterEvent("TALKINGHEAD_CLOSE")
			_G.TalkingHeadFrame:UnregisterEvent("SOUNDKIT_FINISHED")
			_G.TalkingHeadFrame:UnregisterEvent("LOADING_SCREEN_ENABLED")
			_G.TalkingHeadFrame:Hide()
		else
			-- If no frame is found, the addon hasn't been loaded yet,
			-- and it should have been enough to just prevent blizzard from showing it.
			_G.UIParent:UnregisterEvent("TALKINGHEAD_REQUESTED")

			-- Since other addons might load it contrary to our settings, though,
			-- we register our addon listener to take control of it when it's loaded.
			return PK:RegisterEvent("ADDON_LOADED", UpdateTalkingHead)
		end
	end
end

function Module:OnLogin()
	UpdateTalkingHead()
end