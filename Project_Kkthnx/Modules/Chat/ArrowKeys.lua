local _, ns = ...
local PK = unpack(ns)
local Module = PK:GetModule("Chat")

function Module:ChatArrowKeys()
	-- Enable arrow keys for normal and existing chat frames
	for i = 1, 50 do
		if _G["ChatFrame"..i] then
			_G["ChatFrame"..i.."EditBox"]:SetAltArrowKeyMode(false)
		end
	end

	-- Enable arrow keys for temporary chat frames
	hooksecurefunc("FCF_OpenTemporaryWindow", function()
		local cf = FCF_GetCurrentChatFrame():GetName() or nil
		if cf then
			_G[cf.."EditBox"]:SetAltArrowKeyMode(false)
		end
	end)
end