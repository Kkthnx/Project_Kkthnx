local _, ns = ...
local PK = unpack(ns)
local Module = PK:RegisterModule("Chat")

function Module:OnLogin()
	if not ProjectKkthnxChatDB then
		return
	end

	if ProjectKkthnxChatDB.chatEditBoxToTop then
		self:MoveEditBox()
	end

	if ProjectKkthnxChatDB.chatHideButtons then
		self:HideChatButtons()
	end

	if ProjectKkthnxChatDB.chatUnclamp then
		self:UnclampChatFrame()
	end

	if ProjectKkthnxChatDB.chatArrowKeys then
		self:ChatArrowKeys()
	end

	if ProjectKkthnxChatDB.channelRename then
		self:ChannelRename()
	end

	if ProjectKkthnxChatDB.chatCopy then
		self:CreateCopyChat()
	end
end