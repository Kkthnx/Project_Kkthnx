local _, ns = ...
local PK = unpack(ns)
local Module = PK:GetModule("Chat")

local _G = _G
local FCF_GetCurrentChatFrame = _G.FCF_GetCurrentChatFrame
local hooksecurefunc = _G.hooksecurefunc
local IsControlKeyDown = _G.IsControlKeyDown
local IsShiftKeyDown = _G.IsShiftKeyDown

function Module:HideChatButtons()
	-- Function to enable mouse scrolling with CTRL and SHIFT key modifiers
	local function AddMouseScroll(chtfrm)
		if _G[chtfrm] then
			_G[chtfrm]:SetScript("OnMouseWheel", function(self, direction)
				if direction == 1 then
					if IsControlKeyDown() then
						self:ScrollToTop()
					elseif IsShiftKeyDown() then
						self:PageUp()
					else
						self:ScrollUp()
					end
				else
					if IsControlKeyDown() then
						self:ScrollToBottom()
					elseif IsShiftKeyDown() then
						self:PageDown()
					else
						self:ScrollDown()
					end
				end
			end)
			_G[chtfrm]:EnableMouseWheel(true)
		end
	end

	-- Function to hide chat buttons
	local function HideButtons(chtfrm)
		_G[chtfrm.."ButtonFrameMinimizeButton"]:SetParent(PK.UIFrameHider)
		_G[chtfrm.."ButtonFrameMinimizeButton"]:Hide()
		_G[chtfrm.."ButtonFrame"]:SetSize(0.1,0.1)
		_G[chtfrm].ScrollBar:SetParent(PK.UIFrameHider)
		_G[chtfrm].ScrollBar:Hide()
	end

	-- Function to highlight chat tabs and click to scroll to bottom
	local function HighlightTabs(chtfrm)
		-- Set position of bottom button
		_G[chtfrm].ScrollToBottomButton.Flash:SetTexture("Interface/BUTTONS/GRADBLUE.png")
		_G[chtfrm].ScrollToBottomButton:ClearAllPoints()
		_G[chtfrm].ScrollToBottomButton:SetPoint("BOTTOM",_G[chtfrm.."Tab"], 0, -4)
		_G[chtfrm].ScrollToBottomButton:Show()
		_G[chtfrm].ScrollToBottomButton:SetWidth(_G[chtfrm.."Tab"]:GetWidth() - 12)
		_G[chtfrm].ScrollToBottomButton:SetHeight(24)

		-- Resize bottom button according to tab size
		_G[chtfrm.."Tab"]:SetScript("OnSizeChanged", function()
			for j = 1, 50 do
				-- Resize bottom button to tab width
				if _G["ChatFrame"..j] and _G["ChatFrame"..j].ScrollToBottomButton then
					_G["ChatFrame"..j].ScrollToBottomButton:SetWidth(_G["ChatFrame"..j.."Tab"]:GetWidth() - 12)
				end
			end
		end)

		-- Remove click from the bottom button
		_G[chtfrm].ScrollToBottomButton:SetScript("OnClick", nil)

		-- Remove textures
		_G[chtfrm].ScrollToBottomButton:SetNormalTexture("")
		_G[chtfrm].ScrollToBottomButton:SetHighlightTexture("")
		_G[chtfrm].ScrollToBottomButton:SetPushedTexture("")

		-- Always scroll to bottom when clicking a tab
		_G[chtfrm.."Tab"]:HookScript("OnClick", function(_, arg1)
			if arg1 == "LeftButton" then
				_G[chtfrm]:ScrollToBottom()
			end
		end)
	end

	-- Hide chat menu buttons
	ChatFrameMenuButton:SetParent(PK.UIFrameHider)
	ChatFrameChannelButton:SetParent(PK.UIFrameHider)
	ChatFrameToggleVoiceDeafenButton:SetParent(PK.UIFrameHider)
	ChatFrameToggleVoiceMuteButton:SetParent(PK.UIFrameHider)
	QuickJoinToastButton:SetParent(PK.UIFrameHider)

	-- Set options for normal and existing chat frames
	for i = 1, 50 do
		if _G["ChatFrame"..i] then
			AddMouseScroll("ChatFrame"..i)
			HideButtons("ChatFrame"..i)
			HighlightTabs("ChatFrame"..i)
		end
	end

	-- Do the functions above for temporary chat frames
	hooksecurefunc("FCF_OpenTemporaryWindow", function()
		local cf = FCF_GetCurrentChatFrame():GetName() or nil
		if cf then
			-- Set options for temporary frame
			AddMouseScroll(cf)
			HideButtons(cf)
			HighlightTabs(cf)
			-- Resize flashing alert to match tab width
			_G[cf.."Tab"]:SetScript("OnSizeChanged", function()
				_G[cf].ScrollToBottomButton:SetWidth(_G[cf.."Tab"]:GetWidth()-10)
			end)
		end
	end)

end