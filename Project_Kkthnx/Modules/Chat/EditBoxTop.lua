local _, ns = ...
local PK = unpack(ns)
local Module = PK:GetModule("Chat")

local _G = _G
local hooksecurefunc = _G.hooksecurefunc
local FCF_GetCurrentChatFrame = _G.FCF_GetCurrentChatFrame

function Module:MoveEditBox()
	-- Set options for normal chat frames
	for i = 1, 50 do
		if _G["ChatFrame"..i] then
			-- Position the editbox
			_G["ChatFrame"..i.."EditBox"]:ClearAllPoints()
			_G["ChatFrame"..i.."EditBox"]:SetPoint("BOTTOMLEFT", _G["ChatFrame"..i], "TOPLEFT", 2, 26)
			_G["ChatFrame"..i.."EditBox"]:SetPoint("BOTTOMRIGHT", _G["ChatFrame"..i], "TOPRIGHT", 23, 26)
		end
	end

	-- Do the functions above for other chat frames (pet battles, whispers, etc)
	hooksecurefunc("FCF_OpenTemporaryWindow", function()
		local cf = FCF_GetCurrentChatFrame():GetName() or nil
		if cf then
			-- Position the editbox
			_G[cf.."EditBox"]:ClearAllPoints()
			_G[cf.."EditBox"]:SetPoint("BOTTOMLEFT", cf, "TOPLEFT", 2, 26)
			_G[cf.."EditBox"]:SetPoint("BOTTOMRIGHT", cf, "TOPRIGHT", 23, 26)
		end
	end)
end