local _, ns = ...
local PK = unpack(ns)
local Module = PK:GetModule("Chat")

local gsub, strfind = string.gsub, string.find
local INTERFACE_ACTION_BLOCKED = INTERFACE_ACTION_BLOCKED

function Module:UpdateChannelNames(text, ...)
	local r, g, b = ...
	if strfind(text, "Tell".." |H[BN]*player.+%]") then
		r, g, b = r* 0.7, g * 0.7, b * 0.7
	end

	return self.oldAddMsg(self, gsub(text, "|h%[(%d+)%..-%]|h", "|h[%1]|h"), r, g, b)
end

function Module:ChannelRename()
	for i = 1, NUM_CHAT_WINDOWS do
		if i ~= 2 then
			local chatFrame = _G["ChatFrame"..i]
			chatFrame.oldAddMsg = chatFrame.AddMessage
			chatFrame.AddMessage = Module.UpdateChannelNames
		end
	end

	--online/offline info
	ERR_FRIEND_ONLINE_SS = gsub(ERR_FRIEND_ONLINE_SS, "%]%|h", "]|h|cff00c957")
	ERR_FRIEND_OFFLINE_S = gsub(ERR_FRIEND_OFFLINE_S, "%%s", "%%s|cffff7f50")

	--whisper
	CHAT_WHISPER_INFORM_GET = "Tell".." %s "
	CHAT_WHISPER_GET = "From".." %s "
	CHAT_BN_WHISPER_INFORM_GET = "Tell".." %s "
	CHAT_BN_WHISPER_GET = "From".." %s "

	--say / yell
	CHAT_SAY_GET = "%s "
	CHAT_YELL_GET = "%s "

	--guild
	CHAT_GUILD_GET = "|Hchannel:GUILD|h[G]|h %s "
	CHAT_OFFICER_GET = "|Hchannel:OFFICER|h[O]|h %s "

	--raid
	CHAT_RAID_GET = "|Hchannel:RAID|h[R]|h %s "
	CHAT_RAID_WARNING_GET = "[RW] %s "
	CHAT_RAID_LEADER_GET = "|Hchannel:RAID|h[RL]|h %s "

	--party
	CHAT_PARTY_GET = "|Hchannel:PARTY|h[P]|h %s "
	CHAT_PARTY_LEADER_GET = "|Hchannel:PARTY|h[PL]|h %s "
	CHAT_PARTY_GUIDE_GET = "|Hchannel:PARTY|h[PG]|h %s "

	--instance
	CHAT_INSTANCE_CHAT_GET = "|Hchannel:INSTANCE|h[I]|h %s "
	CHAT_INSTANCE_CHAT_LEADER_GET = "|Hchannel:INSTANCE|h[IL]|h %s "

	--flags
	CHAT_FLAG_AFK = "[AFK] "
	CHAT_FLAG_DND = "[DND] "
	CHAT_FLAG_GM = "[GM] "
end