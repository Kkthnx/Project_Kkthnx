local _, ns = ...
local PK = unpack(ns)
local Module = PK:GetModule("Chat")

function Module:UnclampChatFrame()
	-- Process normal and existing chat frames on startup
	for i = 1, 50 do
		if _G["ChatFrame"..i] then
			_G["ChatFrame"..i]:SetClampRectInsets(0, 0, 0, 0)
		end
	end

	-- Process new chat frames and combat log
	hooksecurefunc("FloatingChatFrame_UpdateBackgroundAnchors", function(self)
		self:SetClampRectInsets(0, 0, 0, 0)
	end)

	-- Process temporary chat frames
	hooksecurefunc("FCF_OpenTemporaryWindow", function()
		local cf = FCF_GetCurrentChatFrame():GetName() or nil
		if cf then
			_G[cf]:SetClampRectInsets(0, 0, 0, 0)
		end
	end)
end