-- Minimap original code was from kaytotes"s ImpBlizzardUI
local _, ns = ...
local PK, PKDB = unpack(ns)
local Module = PK:RegisterModule("Minimap")

local _G = _G

-- Local Variables
local coords

-- Local Functions
local Minimap_ZoomIn = Minimap_ZoomIn
local Minimap_ZoomOut = Minimap_ZoomOut
local IsInInstance = IsInInstance

function Module:MinimapOnMouseWheel(_, delta)
	if (delta > 0) then
		Minimap_ZoomIn()
	else
		Minimap_ZoomOut()
	end
end

function Module:StyleClock()
	TimeManagerClockTicker:SetFont(PKDB.Font[1], PKDB.Font[2] + 1, "OUTLINE")
	TimeManagerClockTicker:SetShadowOffset(0, 0)
end

function Module:StyleMap()
	MinimapCluster:ClearAllPoints()

	if (ProjectKkthnxMinimapDB.minimapPerformance) then
		MinimapCluster:SetPoint("TOPRIGHT", 0, -11)
		MinimapBorderTop:Kill()
		MiniMapWorldMapButton:Kill()
		GameTimeFrame:SetScale(0.9)
	else
		MinimapCluster:SetPoint("TOPRIGHT", 0, -4)
	end

	-- Replace Zoom Buttons with Mousewheel.
	MinimapZoomIn:Hide()
	MinimapZoomOut:Hide()
	Minimap:EnableMouseWheel(true)

	Minimap:SetScript("OnMouseWheel", function(self, delta)
		Module:MinimapOnMouseWheel(self, delta)
	end)

	MinimapZoneText:ClearAllPoints()
	MinimapZoneText:SetPoint("TOP", MinimapCluster, "TOP", 8, -7)
	MinimapZoneText:SetFont(PKDB.Font[1], PKDB.Font[2] + 1, "OUTLINE")
	MinimapZoneText:SetShadowOffset(0, 0)
end

function Module:StyleCoords()
	coords.text:SetFont(PKDB.Font[1], PKDB.Font[2] + 1, "OUTLINE")
	coords.text:SetTextColor(1, 1, 0, 1)
	coords.text:SetShadowOffset(0, 0)
end

function Module:UpdateCoords(elapsed)
	coords.elapsed = coords.elapsed + elapsed
	local inInstance, instanceType = IsInInstance()

	-- If configuration disabled or we"re in a dungeon then just set to blank.
	if (coords.elapsed >= coords.delay) then
		if (ProjectKkthnxMinimapDB.minimapCoords == false or inInstance == true) then
			coords.text:SetText("")
			return
		end

		local map = C_Map.GetBestMapForUnit("player")

		if (map) then
			if (Minimap:IsVisible()) then
				local x, y = C_Map.GetPlayerMapPosition(map, "player"):GetXY()
				if (x ~= 0 and y ~= 0) then
					coords.text:SetFormattedText("(%d:%d)", x * 100, y * 100)
				else
					coords.text:SetText("")
				end
			end
		else
			if (Minimap:IsVisible()) then
				coords.text:SetText("")
			end
		end

		coords.elapsed = 0
	end
end

function Module:UpdatePerformance(elapsed)
	performance.elapsed = performance.elapsed + elapsed
	-- Bail out if configuration option is disabled.

	if (performance.elapsed >= performance.delay) then
		if (ProjectKkthnxMinimapDB.minimapPerformance == false) then
			performance.text:SetText(" ")
			return
		end

		local _, _, latencyHome, latencyWorld = GetNetStats() -- Get current Latency

		-- Colour Latency Strings
		if (latencyHome <= 75 )then
			latencyHome = format("|cff00CC00%s|r", latencyHome)
		elseif (latencyHome > 75 and latencyHome <= 250 )then
			latencyHome = format("|cffFFFF00%s|r", latencyHome)
		elseif (latencyHome > 250 )then
			latencyHome = format("|cffFF0000%s|r", latencyHome)
		end

		if (latencyWorld <= 75 )then
			latencyWorld = format("|cff00CC00%s|r", latencyWorld)
		elseif (latencyWorld > 75 and latencyWorld <= 250 )then
			latencyWorld = format("|cffFFFF00%s|r", latencyWorld)
		elseif (latencyWorld > 250 )then
			latencyWorld = format("|cffFF0000%s|r", latencyWorld)
		end

		local frameRate = floor(GetFramerate()) -- Get the current frame rate

		-- Colour Frame Rate
		if (frameRate >= 59) then
			frameRate = format("|cff00CC00%s|r", frameRate )
		elseif (frameRate >= 20 and frameRate <= 58) then
			frameRate = format("|cffFFFF00%s|r", frameRate )
		elseif (frameRate < 20) then
			frameRate = format("|cffFF0000%s|r", frameRate )
		end

		-- Write Text
		performance.text:SetText(latencyHome.." / "..latencyWorld.." ms - "..frameRate.." fps")

		performance.elapsed = 0
	end
end

function Module:StylePerformanceFrame()
    performance.text:SetFont(PKDB.Font[1], PKDB.Font[2] + 1, "OUTLINE")
    performance.text:SetShadowOffset(0, 0)
end

function Module:OnLogin()
	-- Create Coords Frame
	coords = CreateFrame("Frame", nil, Minimap)
	coords.text = coords:CreateFontString(nil, "OVERLAY", "GameFontNormal")
	coords.delay = 0.6
	coords.elapsed = 0

	-- Set the position and scale etc
	coords:SetFrameStrata("LOW")
	coords:SetWidth(32)
	coords:SetHeight(32)
	coords:SetPoint("BOTTOM", 3, 0)
	coords.text:SetPoint("CENTER", 0, 0)

	Module:StyleMap()
	Module:StyleCoords()
	Module:StyleClock()

	coords:SetScript("OnUpdate", self.UpdateCoords)

	-- Create the Performance Frame.
    performance = CreateFrame("Frame", nil, MinimapCluster)
    performance.text = performance:CreateFontString(nil, "OVERLAY", "GameFontNormal")
    performance.delay = 2
	performance.elapsed = 0

    performance:SetFrameStrata("BACKGROUND")
    performance:SetWidth(32)
    performance:SetHeight(32)
    performance:SetPoint("TOP", 10, 18)

    -- Text positioning
    performance.text:SetPoint("CENTER", 0, 0)

    Module:StylePerformanceFrame()

    performance:SetScript("OnUpdate", self.UpdatePerformance)
end