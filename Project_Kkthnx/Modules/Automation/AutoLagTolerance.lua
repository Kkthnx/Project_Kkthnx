local _, ns = ...
local PK = unpack(ns)
local Module = PK:RegisterModule("AutoLagTolerance")

local _G = _G

function Module:OnLogin()
	if ProjectKkthnxAutomationDB.autoLagTolerance then
		local customlag = _G.CreateFrame("Frame")
		local int = 5
		local _, _, _, lag = _G.GetNetStats()

		local LatencyUpdate = function(_, elapsed)
			int = int - elapsed
			if int < 0 then
				if lag ~= 0 and lag <= 400 then
					if not _G.InCombatLockdown() then
						_G.SetCVar("SpellQueueWindow", _G.tostring(lag))
					end
				end
				int = 5
			end
		end

		customlag:SetScript("OnUpdate", LatencyUpdate)
		LatencyUpdate(customlag, 10)
	end
end