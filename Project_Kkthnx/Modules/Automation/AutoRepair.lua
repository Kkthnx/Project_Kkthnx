local _, ns = ...
local PK, PKDB = unpack(ns)

local _G = _G

local isShown, isBankEmpty, autoRepair, repairAllCost, canRepair

local function delayFunc()
	if isBankEmpty then
		autoRepair(true)
	else
		PK.Print(_G.format(PKDB.InfoColor.."%s:|r %s", "Guild repair", PK.FormatMoney(repairAllCost)))
	end
end

function autoRepair(override)
	if isShown and not override then
		return
	end

	isShown = true
	isBankEmpty = false

	local myMoney = _G.GetMoney()
	repairAllCost, canRepair = _G.GetRepairAllCost()

	if canRepair and repairAllCost > 0 then
		if (not override) and _G.ProjectKkthnxAutomationDB.autoRepair == 2 and _G.IsInGuild() and _G.CanGuildBankRepair() and _G.GetGuildBankWithdrawMoney() >= repairAllCost then
			_G.RepairAllItems(true)
		elseif _G.ProjectKkthnxAutomationDB.autoRepair == 1 then
			if myMoney > repairAllCost then
				_G.RepairAllItems()
				PK.Print(_G.format(PKDB.InfoColor.."%s:|r %s", "Repair cost", PK.FormatMoney(repairAllCost)))
				return
			else
				PK.Print(PKDB.InfoColor.."Repair error")
				return
			end
		end

		_G.C_Timer.After(.5, delayFunc)
	end
end

local function checkBankFund(_, msgType)
	if msgType == _G.LE_GAME_ERR_GUILD_NOT_ENOUGH_MONEY then
		isBankEmpty = true
	end
end

local function merchantClose()
	isShown = false
	PK:UnregisterEvent("UI_ERROR_MESSAGE", checkBankFund)
	PK:UnregisterEvent("MERCHANT_CLOSED", merchantClose)
end

local function merchantShow()
	if _G.IsShiftKeyDown() or _G.ProjectKkthnxAutomationDB.autoRepair == 0 or not _G.CanMerchantRepair() then
		return
	end

	autoRepair()
	PK:RegisterEvent("UI_ERROR_MESSAGE", checkBankFund)
	PK:RegisterEvent("MERCHANT_CLOSED", merchantClose)
end
PK:RegisterEvent("MERCHANT_SHOW", merchantShow)