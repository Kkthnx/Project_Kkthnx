local _, ns = ...
local PK = unpack(ns)
local Module = PK:RegisterModule("AutoDeclineDuel")

local DuelCanceled_Pet = "Pet duel request from %s rejected."
local DuelCanceled_Regular = "Duel request from %s rejected."

local _G = _G

local CancelDuel = _G.CancelDuel
local CancelPetPVPDuel = _G.CancelPetPVPDuel
local StaticPopup_Hide = _G.StaticPopup_Hide

-- Auto decline duels
function Module:OnLogin(event, name)
	local cancelled = false
	if event == "DUEL_REQUESTED" and ProjectKkthnxAutomationDB.declinePvPDuel then
		CancelDuel()
		StaticPopup_Hide("DUEL_REQUESTED")
		cancelled = "Regular"
	elseif event == "PET_BATTLE_PVP_DUEL_REQUESTED" and ProjectKkthnxAutomationDB.declinePetDuel then
		CancelPetPVPDuel()
		StaticPopup_Hide("PET_BATTLE_PVP_DUEL_REQUESTED")
		cancelled = "Pet"
	end

	if cancelled and cancelled == "Pet" then
		PK.Print(_G.string.format(DuelCanceled_Pet, "|cff4488ff"..name.."|r"))
	elseif cancelled and cancelled == "Regular" then
		PK.Print(_G.string.format(DuelCanceled_Regular, "|cff4488ff"..name.."|r"))
	end
end

local cif = CreateFrame("Frame", nil, CastingBarFrame)
cif:SetSize(32, 32)
cif:SetFrameStrata("BACKGROUND")
cif:SetPoint("RIGHT", CastingBarFrameText, "LEFT", -15, 0)

local cift = cif:CreateTexture(nil,"BACKGROUND")
cift:SetAllPoints(true)

cif:SetScript("OnEvent",function(self)
 --local _, _, _, icon = UnitCastingInfo("player")
 local name, text, texture, startTimeMS, endTimeMS, isTradeSkill, castID, notInterruptible, spellId = UnitCastingInfo("player")
 cift:SetTexture(castID)
end)
cif:RegisterEvent("UNIT_SPELLCAST_CHANNEL_START")
cif:RegisterEvent("UNIT_SPELLCAST_START")