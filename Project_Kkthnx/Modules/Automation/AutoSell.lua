local _, ns = ...
local PK = unpack(ns)

local _G = _G

local sellCount, stop, cache = 0, true, {}
local errorText = _G.ERR_VENDOR_DOESNT_BUY

local function stopSelling(tell)
	stop = true
	if sellCount > 0 and tell then
		PK.Print(_G.format("|cff99CCFF%s|r %s", "Junk Sold For:", PK.FormatMoney(sellCount)))
	end

	sellCount = 0
end

local function startSelling()
	if stop then
		return
	end

	for bag = 0, 4 do
		for slot = 1, _G.GetContainerNumSlots(bag) do
			if stop then
				return
			end

			local link = _G.GetContainerItemLink(bag, slot)
			if link then
				local price = _G.select(11, _G.GetItemInfo(link))
				local _, count, _, quality = _G.GetContainerItemInfo(bag, slot)
				if quality == 0 and price > 0 and not cache["b"..bag.."s"..slot] then
					sellCount = sellCount + price * count
					cache["b"..bag.."s"..slot] = true
					_G.UseContainerItem(bag, slot)
					_G.C_Timer.After(.2, startSelling)
					return
				end
			end
		end
	end
end

local function updateSelling(event, ...)
	if not _G.ProjectKkthnxAutomationDB.autoSell then
		return
	end

	local _, arg = ...
	if event == "MERCHANT_SHOW" then
		if _G.IsShiftKeyDown() then
			return
		end

		stop = false
		_G.wipe(cache)
		startSelling()
		PK:RegisterEvent("UI_ERROR_MESSAGE", updateSelling)
	elseif event == "UI_ERROR_MESSAGE" and arg == errorText then
		stopSelling(false)
	elseif event == "MERCHANT_CLOSED" then
		stopSelling(true)
	end
end
PK:RegisterEvent("MERCHANT_SHOW", updateSelling)
PK:RegisterEvent("MERCHANT_CLOSED", updateSelling)