local _, ns = ...
local PK, PKDB = unpack(ns)
local Module = PK:RegisterModule("Developer")

-- This File Will Serve As Testing For The Time Being. If Something Works Out It Will Get Implemented.

local _G = _G

local GameTooltip = _G.GameTooltip
local InCombatLockdown = _G.InCombatLockdown
local IsShiftKeyDown = _G.IsShiftKeyDown
local CreateFrame = _G.CreateFrame
local PlayerFrame = _G.PlayerFrame
local TargetFrame = _G.TargetFrame
local CastingBarFrame = _G.CastingBarFrame
local UIParent = _G.UIParent

local testModules = CreateFrame("Frame")
testModules:RegisterEvent("ADDON_LOADED")
testModules:SetScript("OnEvent", function()
	local objectiveTrackerFrame = _G["ObjectiveTrackerFrame"]
	objectiveTrackerFrame:SetHeight(600)
	objectiveTrackerFrame:SetClampedToScreen(false)
	objectiveTrackerFrame:SetMovable(true)
    objectiveTrackerFrame:SetUserPlaced(true)

	local minimizeButton = objectiveTrackerFrame.HeaderMenu.MinimizeButton
	minimizeButton:EnableMouse(true)
	minimizeButton:RegisterForDrag("LeftButton")
	minimizeButton:SetHitRectInsets(-15, 0, -5, -5)
	minimizeButton:SetScript("OnDragStart", function()
		if (IsShiftKeyDown()) then
			objectiveTrackerFrame:StartMoving()
		end
	end)

	minimizeButton:SetScript("OnDragStop", function()
		objectiveTrackerFrame:StopMovingOrSizing()
	end)

	minimizeButton:SetScript("OnEnter", function()
		if GameTooltip:IsForbidden() then
			return
		end

		if (not InCombatLockdown()) then
			GameTooltip:SetOwner(minimizeButton, "ANCHOR_TOPLEFT", 0, 10)
			GameTooltip:ClearLines()
			GameTooltip:AddLine("Shift + Left-Click To Drag")
			GameTooltip:Show()
		end
	end)

	minimizeButton:SetScript("OnLeave", function()
		GameTooltip:Hide()
	end)

	local function SkinOjectiveTrackerHeaders()
		local frame = objectiveTrackerFrame.MODULES

		if frame then
			for i = 1, #frame do
				local modules = frame[i]
				if modules then
					local header = modules.Header

					local background = modules.Header.Background
					background:SetAtlas(nil)

					local text = modules.Header.Text
					text:SetTextColor(255/255, 210/255, 0/255)

					if not modules.IsSkinned then
						local headerPanel = CreateFrame("Frame", nil, header)
						headerPanel:SetFrameLevel(header:GetFrameLevel() - 1)
						headerPanel:SetFrameStrata("BACKGROUND")
						headerPanel:SetPoint("TOPLEFT", 1, 1)
						headerPanel:SetPoint("BOTTOMRIGHT", 1, 1)

						local headerBar = headerPanel:CreateTexture(nil, "ARTWORK")
						headerBar:SetTexture("Interface\\LFGFrame\\UI-LFG-SEPARATOR")
						headerBar:SetTexCoord(0, 0.6640625, 0, 0.3125)
						headerBar:SetVertexColor(255/255, 210/255, 0/255)
						headerBar:SetPoint("CENTER", headerPanel, -20, -4)
						headerBar:SetSize(232, 30)

						modules.IsSkinned = true
					end
				end
			end
		end
    end
    hooksecurefunc("ObjectiveTracker_Update", SkinOjectiveTrackerHeaders)
end)

function Module:OnLogin()
    -- -- Parent PlayerFrame to the Drag Frame.
    -- PlayerFrame:SetMovable(true)
    -- PlayerFrame:ClearAllPoints()
    -- PlayerFrame:SetPoint("BOTTOM", UIParent, "BOTTOM", -290, 320)
    -- PlayerFrame:SetScale(1.0)
    -- PlayerFrame:SetUserPlaced(true)
    -- PlayerFrame:SetMovable(false)

    -- -- Parent Target Frame to the Drag Frame.
    -- TargetFrame:SetMovable(true)
    -- TargetFrame:ClearAllPoints()
    -- TargetFrame:SetPoint("BOTTOM", UIParent, "BOTTOM", 290, 320)
    -- TargetFrame:SetScale(1.0)
    -- TargetFrame:SetUserPlaced(true)
    -- TargetFrame:SetMovable(false)

    -- CastingBarFrame:SetMovable(true)
    -- CastingBarFrame:ClearAllPoints()
    -- CastingBarFrame:SetPoint("BOTTOM", UIParent, "BOTTOM", 0, 320)
    -- CastingBarFrame:SetScale(1.0)
    -- CastingBarFrame:SetUserPlaced(true)
    -- CastingBarFrame:SetMovable(false)

    -- CastingBarFrame.Icon:Show()
    -- CastingBarFrame.Icon:SetSize(24, 24)
    -- CastingBarFrame.Icon:ClearAllPoints()
    -- CastingBarFrame.Icon:SetPoint("RIGHT", CastingBarFrame, "LEFT", -8, 2)

   -- SetCVar("ActionButtonUseKeyDown", 1)
end