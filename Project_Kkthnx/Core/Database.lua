local _, ns = ...
local _, PKDB = unpack(ns)

local _G = _G

PKDB.Version = _G.GetAddOnMetadata("Project_Kkthnx", "Version")
PKDB.Support = _G.GetAddOnMetadata("Project_Kkthnx", "X-Support")
PKDB.Client = _G.GetLocale()
PKDB.ScreenWidth, PKDB.ScreenHeight = _G.GetPhysicalScreenSize()

-- Colors
PKDB.MyName = _G.UnitName("player")
PKDB.MyRealm = _G.GetRealmName()
PKDB.MyClass = _G.select(2, _G.UnitClass("player"))
PKDB.ClassList = {}
for k, v in _G.pairs(_G.LOCALIZED_CLASS_NAMES_MALE) do
	PKDB.ClassList[v] = k
end

PKDB.ClassColors = {}
local colors = _G.CUSTOM_CLASS_COLORS or _G.RAID_CLASS_COLORS
for class in _G.pairs(colors) do
	PKDB.ClassColors[class] = {}
	PKDB.ClassColors[class].r = colors[class].r
	PKDB.ClassColors[class].g = colors[class].g
	PKDB.ClassColors[class].b = colors[class].b
	PKDB.ClassColors[class].colorStr = colors[class].colorStr
end
PKDB.r, PKDB.g, PKDB.b = PKDB.ClassColors[PKDB.MyClass].r, PKDB.ClassColors[PKDB.MyClass].g, PKDB.ClassColors[PKDB.MyClass].b
PKDB.MyColor = _G.format("|cff%02x%02x%02x", PKDB.r * 255, PKDB.g * 255, PKDB.b * 255)
PKDB.TexCoords = {.08, .92, .08, .92}
PKDB.InfoColor = "|cff4488ff"
PKDB.GreyColor = "|cff7b8489"

-- Fonts
PKDB.Font = {_G.STANDARD_TEXT_FONT, 12, "OUTLINE"}
PKDB.TipFont = {_G.GameTooltipText:GetFont(), 14, "OUTLINE"}
PKDB.LineString = PKDB.GreyColor.."---------------"