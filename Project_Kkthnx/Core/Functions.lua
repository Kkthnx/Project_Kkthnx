local addonName, ns = ...
local PK = unpack(ns)

local _G = _G

PK.UIFrameHider = _G.CreateFrame("Frame", "UIFrameHider", _G.UIParent)
PK.UIFrameHider:Hide()
PK.UIFrameHider:SetAllPoints()
PK.UIFrameHider.children = {}
_G.RegisterStateDriver(PK.UIFrameHider, "visibility", "hide")

PK.PetBattleHider = _G.CreateFrame("Frame", "PetBattleHider", _G.UIParent, "SecureHandlerStateTemplate")
PK.PetBattleHider:SetAllPoints()
PK.PetBattleHider:SetFrameStrata("LOW")
_G.RegisterStateDriver(PK.PetBattleHider, "visibility", "[petbattle] hide; show")

function PK.Print(...)
	_G.print("|cff3c9bed".._G.GetAddOnMetadata(addonName, "Title").."|r:", ...)
end

function PK.FormatMoney(amount)
	local coppername = "|cffeda55fc|r"
	local silvername = "|cffc7c7cfs|r"
	local goldname = "|cffffd700g|r"
	local value = _G.math.abs(amount)
	local gold = _G.math.floor(value / 10000)
	local silver = _G.math.floor(_G.mod(value / 100, 100))
	local copper = _G.math.floor(_G.mod(value, 100))

	local str = ""
	if gold > 0 then
		str = _G.string.format("%d%s%s", gold, goldname, (silver > 0 or copper > 0) and " " or "")
	end

	if silver > 0 then
		str = _G.string.format("%s%d%s%s", str, silver, silvername, copper > 0 and " " or "")
	end

	if copper > 0 or value == 0 then
		str = _G.string.format("%s%d%s", str, copper, coppername)
	end

	return str
end

function PK.RGBToHex(r, g, b)
	r = r <= 1 and r >= 0 and r or 0
	g = g <= 1 and g >= 0 and g or 0
	b = b <= 1 and b >= 0 and b or 0

	return string.format("|cff%02x%02x%02x", r * 255, g * 255, b * 255)
end

function PK.ShortValue(value)
	if value >= 1e11 then
		return ("%.0fb"):format(value / 1e9)
	elseif value >= 1e10 then
		return ("%.1fb"):format(value / 1e9):gsub("%.?0+([km])$", "%1")
	elseif value >= 1e9 then
		return ("%.2fb"):format(value / 1e9):gsub("%.?0+([km])$", "%1")
	elseif value >= 1e8 then
		return ("%.0fm"):format(value / 1e6)
	elseif value >= 1e7 then
		return ("%.1fm"):format(value / 1e6):gsub("%.?0+([km])$", "%1")
	elseif value >= 1e6 then
		return ("%.2fm"):format(value / 1e6):gsub("%.?0+([km])$", "%1")
	elseif value >= 1e5 then
		return ("%.0fk"):format(value / 1e3)
	elseif value >= 1e3 then
		return ("%.1fk"):format(value / 1e3):gsub("%.?0+([km])$", "%1")
	else
		return value
	end
end

-- Itemlevel
local iLvlDB = {}
local itemLevelString = _G["ITEM_LEVEL"]:gsub("%%d", "")
local tip = CreateFrame("GameTooltip", "ProjectKkthnx_iLvlTooltip", nil, "GameTooltipTemplate")

function PK.GetItemLevel(link, arg1, arg2)
	if iLvlDB[link] then return iLvlDB[link] end

	tip:SetOwner(UIParent, "ANCHOR_NONE")
	if arg1 and type(arg1) == "string" then
		tip:SetInventoryItem(arg1, arg2)
	elseif arg1 and type(arg1) == "number" then
		tip:SetBagItem(arg1, arg2)
	else
		tip:SetHyperlink(link)
	end

	for i = 2, 5 do
		local text = _G[tip:GetName().."TextLeft"..i]:GetText() or ""
		local found = strfind(text, itemLevelString)
		if found then
			local level = strmatch(text, "(%d+)%)?$")
			iLvlDB[link] = tonumber(level)
			break
		end
	end
	return iLvlDB[link]
end

local function Kill(object)
	if object.UnregisterAllEvents then
		object:UnregisterAllEvents()
		object:SetParent(PK.UIFrameHider)
	else
		object.Show = object.Hide
	end

	object:Hide()
end

local StripTexturesBlizzFrames = {
	"Inset",
	"inset",
	"InsetFrame",
	"LeftInset",
	"RightInset",
	"NineSlice",
	"BG",
	"border",
	"Border",
	"BorderFrame",
	"bottomInset",
	"BottomInset",
	"bgLeft",
	"bgRight",
	"FilligreeOverlay",
	"PortraitOverlay",
	"ArtOverlayFrame",
	"Portrait",
	"portrait",
}

local STRIP_TEX = "Texture"
local STRIP_FONT = "FontString"
local function StripRegion(which, object, kill, alpha)
	if kill then
		object:Kill()
	elseif alpha then
		object:SetAlpha(0)
	elseif which == STRIP_TEX then
		object:SetTexture()
	elseif which == STRIP_FONT then
		object:SetText("")
	end
end

local function StripType(which, object, kill, alpha)
	if object:IsObjectType(which) then
		StripRegion(which, object, kill, alpha)
	else
		if which == STRIP_TEX then
			local FrameName = object.GetName and object:GetName()
			for _, Blizzard in _G.pairs(StripTexturesBlizzFrames) do
				local BlizzFrame = object[Blizzard] or (FrameName and _G[FrameName..Blizzard])
				if BlizzFrame then
					BlizzFrame:StripTextures(kill, alpha)
				end
			end
		end

		if object.GetNumRegions then
			for i = 1, object:GetNumRegions() do
				local region = _G.select(i, object:GetRegions())
				if region and region.IsObjectType and region:IsObjectType(which) then
					StripRegion(which, region, kill, alpha)
				end
			end
		end
	end
end

local function StripTextures(object, kill, alpha)
	StripType(STRIP_TEX, object, kill, alpha)
end

local function StripTexts(object, kill, alpha)
	StripType(STRIP_FONT, object, kill, alpha)
end

local function AddCustomAPI(object)
	local MetaTable = _G.getmetatable(object).__index

	if not object.Kill then
		MetaTable.Kill = Kill
	end

	if not object.StripTextures then
		MetaTable.StripTextures = StripTextures
	end

	if not object.StripTexts then
		MetaTable.StripTexts = StripTexts
	end
end

local Handled = {["Frame"] = true}
local Object = _G.CreateFrame("Frame")

AddCustomAPI(Object)
AddCustomAPI(Object:CreateTexture())
AddCustomAPI(Object:CreateFontString())

Object = _G.EnumerateFrames()
while Object do
	if not Object:IsForbidden() and not Handled[Object:GetObjectType()] then
		AddCustomAPI(Object)
		Handled[Object:GetObjectType()] = true
	end

	Object = _G.EnumerateFrames(Object)
end

-- Hacky fix for issue on 7.1 PTR where scroll frames no longer seem to inherit the methods from the "Frame" widget
local ScrollFrame = _G.CreateFrame("ScrollFrame")
AddCustomAPI(ScrollFrame)