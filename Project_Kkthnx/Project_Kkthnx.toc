## Interface: 80000
## Author: Kkthnx
## Version: 1.05
## Title: |cff4488ffProject Kkthnx|r
## Notes: A Handful Of Quality Of Life Editions For The Default World of Warcraft Interface
## OptionalDeps: Wasabi
## SavedVariables: ProjectKkthnxActionbarDB, ProjectKkthnxAutomationDB, ProjectKkthnxGeneralDB, ProjectKkthnxTooltipDB, ProjectKkthnxMinimapDB, ProjectKkthnxChatDB, ProjectKkthnxUnitframeDB

Libraries\LibStub\LibStub.lua
Libraries\Wasabi\embed.xml

Locales\Core.lua

Init.lua

Core\Database.lua
Core\Functions.lua

Config\Settings.lua

Modules\Actionbar\ButtonRange.lua
Modules\Actionbar\NoGryphons.lua

Modules\Automation\AutoDeclineDuel.lua
Modules\Automation\AutoLagTolerance.lua
Modules\Automation\AutoRepair.lua
Modules\Automation\AutoSell.lua

Modules\Chat\Core.lua
Modules\Chat\ArrowKeys.lua
Modules\Chat\ChannelRename.lua
Modules\Chat\EditBoxTop.lua
Modules\Chat\HideChatButtons.lua
Modules\Chat\UnclampChat.lua
Modules\Chat\CopyChat.lua

Modules\General\CharacterFrame.lua
Modules\General\FixGarbageCollection.lua
Modules\General\InspectFrame.lua
Modules\General\ItemLevel.lua
Modules\General\NoTalkingHead.lua

Modules\Minimap\Minimap.lua

Modules\Tooltip\Core.lua
Modules\Tooltip\TooltipIcons.lua
Modules\Tooltip\ItemLevelSpec.lua
Modules\Tooltip\Style.lua

Modules\Unitframes\Core.lua
Modules\Unitframes\ClassColorHealth.lua
Modules\Unitframes\ClassPortraits.lua

Core\Developer.lua