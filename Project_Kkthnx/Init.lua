local addonName, ns = ...
ns[1] = {} -- PK, Basement
ns[2] = {} -- PKDB, Database

ProjectKkthnxActionbarDB = {}
ProjectKkthnxAutomationDB = {}
ProjectKkthnxGeneralDB = {}
ProjectKkthnxTooltipDB = {}
ProjectKkthnxMinimapDB = {}
ProjectKkthnxChatDB = {}
ProjectKkthnxUnitframeDB = {}

local _G = _G

local PK = _G.unpack(ns)
local pairs, next, tinsert = _G.pairs, _G.next, _G.table.insert

-- Events
local events = {}

local host = _G.CreateFrame("Frame")
host:SetScript("OnEvent", function(_, event, ...)
	for func in pairs(events[event]) do
		if event == "COMBAT_LOG_EVENT_UNFILTERED" then
			func(event, _G.CombatLogGetCurrentEventInfo())
		else
			func(event, ...)
		end
	end
end)

function PK:RegisterEvent(event, func, unit1, unit2)
	if not events[event] then
		events[event] = {}
		if unit1 then
			host:RegisterUnitEvent(event, unit1, unit2)
		else
			host:RegisterEvent(event)
		end
	end

	events[event][func] = true
end

function PK:UnregisterEvent(event, func)
	local funcs = events[event]
	if funcs and funcs[func] then
		funcs[func] = nil

		if not next(funcs) then
			events[event] = nil
			host:UnregisterEvent(event)
		end
	end
end

-- Modules
local modules, initQueue = {}, {}

function PK:RegisterModule(name)
	if modules[name] then _G.print("Module <"..name.."> has been registered.") return end
	local module = {}
	module.name = name
	modules[name] = module

	tinsert(initQueue, module)
	return module
end

function PK:GetModule(name)
	if not modules[name] then _G.print("Module <"..name.."> does not exist.") return end

	return modules[name]
end

-- Init
PK:RegisterEvent("PLAYER_LOGIN", function()
	for _, module in next, initQueue do
		if module.OnLogin then
			module:OnLogin()
		else
			_G.print("Module <"..module.name.."> does not loaded.")
		end
	end

	PK.Modules = modules
end)

_G[addonName] = ns

-- we want this in all versions
_G.SLASH_RELOADUI1 = "/rl"
_G.SLASH_RELOADUI2 = "/reload"
_G.SLASH_RELOADUI3 = "/reloadui"
_G.SLASH_RELOADUI4 = "//"
_G.SLASH_RELOADUI5 = "/."
_G.SlashCmdList.RELOADUI = _G.ReloadUI