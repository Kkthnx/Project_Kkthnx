local _, ns = ...
ns.L = {}

local _G = _G

local GetLocale = _G.GetLocale
local setmetatable = _G.setmetatable
local tostring = _G.tostring

local localizations = {}
local locale = GetLocale()

setmetatable(ns.L, {
	__call = function(_, newLocale)
		localizations[newLocale] = {}
		return localizations[newLocale]
	end,
	__index = function(_, key)
		local localeTable = localizations[locale]
		return localeTable and localeTable[key] or tostring(key)
	end
})