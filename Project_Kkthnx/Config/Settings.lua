local addonName, ns = ...
local L = ns.L

local _G = _G

local GeneralDefaults = {
	characterFrame = false,
	fixGarbageCollection = false,
	inspectFrame = false,
	itemLevel = false,
	noTalkingHead = false
}

local ActionbarDefaults = {
	buttonRange = false,
	noGryphons = false
}

local AutomationDefaults = {
	autoLagTolerance = false,
	autoRepair = 0,
	autoSell = false,
	declinePetDuel = false,
	declinePvPDuel = false
}

local TooltipDefaults = {
	tooltipIcons = false,
	tooltipSpecItemLevel = false,
	tooltipSpecItemLevelByShift = false,
	tooltipStyle = false
}

local MinimapDefaults = {
	minimapCoords = false,
	minimapPerformance = false
}

local ChatDefaults = {
	channelRename = false,
	chatArrowKeys = false,
	chatCopy = false,
	chatEditBoxToTop = false,
	chatHideButtons = false,
	chatUnClamp = false
}

local UnitframeDefaults = {
	classColorHealth = false,
	classPortraits = false
}

local General = _G.LibStub("Wasabi"):New(addonName, "ProjectKkthnxGeneralDB", GeneralDefaults)
General:AddSlash("/pkk")
General:Initialize(function(self)
	local Title = self:CreateTitle()
	Title:SetPoint("TOPLEFT", 16, -16)
	Title:SetFormattedText(L["General"])

	local Description = self:CreateDescription()
	Description:SetPoint("TOPLEFT", Title, "BOTTOMLEFT", 0, -8)
	Description:SetPoint("RIGHT", -32, 0)
	Description:SetText(L["General Quality Of Life Tweaks"])

	local CharacterFrame = self:CreateCheckButton("characterFrame")
	CharacterFrame:SetPoint("TOPLEFT", Description, "BOTTOMLEFT", 0, -8)
	CharacterFrame:SetText(L["Extend The CharacterFrame Visually"])

	local InspectFrame = self:CreateCheckButton("inspectFrame")
	InspectFrame:SetPoint("TOPLEFT", CharacterFrame, "BOTTOMLEFT", 0, -8)
	InspectFrame:SetText(L["Extend The InspectFrame Visually"])

	local NoTalkingHead = self:CreateCheckButton("noTalkingHead")
	NoTalkingHead:SetPoint("TOPLEFT", InspectFrame, "BOTTOMLEFT", 0, -8)
	NoTalkingHead:SetText(L["Turn Off The TalkingHead Frame"])

	local FixGarbageCollection = self:CreateCheckButton("fixGarbageCollection")
	FixGarbageCollection:SetPoint("TOPLEFT", NoTalkingHead, "BOTTOMLEFT", 0, -8)
	FixGarbageCollection:SetText(L["Fix Garbage Collection (Recommended)"])

	local ItemLevel = self:CreateCheckButton("itemLevel")
	ItemLevel:SetPoint("TOPLEFT", FixGarbageCollection, "BOTTOMLEFT", 0, -8)
	ItemLevel:SetText(L["Show Item Level On Slots"])
end)

local Actionbar = General:CreateChild("Actionbar", "ProjectKkthnxActionbarDB", ActionbarDefaults)
Actionbar:Initialize(function(self)
	local Title = self:CreateTitle()
	Title:SetPoint("TOPLEFT", 16, -16)
	Title:SetFormattedText("Actionbar")

	local Description = self:CreateDescription()
	Description:SetPoint("TOPLEFT", Title, "BOTTOMLEFT", 0, -8)
	Description:SetPoint("RIGHT", -32, 0)
	Description:SetText(L["Actionbar Quality Of Life Tweaks"])

	local NoGryphons = self:CreateCheckButton("noGryphons")
	NoGryphons:SetPoint("TOPLEFT", Description, "BOTTOMLEFT", 0, -8)
	NoGryphons:SetText(L["Remove The Gryphons From Both Sides Of The Actionbar"])

	local ButtonRange = self:CreateCheckButton("buttonRange")
	ButtonRange:SetPoint("TOPLEFT", NoGryphons, "BOTTOMLEFT", 0, -8)
	ButtonRange:SetText(L["Color Action Buttons When Out Of Range And Out Of Mana"])
end)

local Automation = General:CreateChild("Automation", "ProjectKkthnxAutomationDB", AutomationDefaults)
Automation:Initialize(function(self)
	local Title = self:CreateTitle()
	Title:SetPoint("TOPLEFT", 16, -16)
	Title:SetFormattedText(L["Automation"])

	local Description = self:CreateDescription()
	Description:SetPoint("TOPLEFT", Title, "BOTTOMLEFT", 0, -8)
	Description:SetPoint("RIGHT", -32, 0)
	Description:SetText(L["Automation Quality Of Life Tweaks"])

	local AutoRepair = self:CreateDropDown("autoRepair")
	AutoRepair:SetPoint("TOPLEFT", Description, "BOTTOMLEFT", 0, -8)
	AutoRepair:SetText(L["Auto Repair Your Gear"])
	AutoRepair:SetValues({
		[0] = "None",
		[1] = "Player",
		[2] = "Guild"
	})

	local AutoSell = self:CreateCheckButton("autoSell")
	AutoSell:SetPoint("TOPLEFT", AutoRepair, "BOTTOMLEFT", 0, -8)
	AutoSell:SetText(L["Auto Sell Your Junk"])

	local AutoLagTolerance = self:CreateCheckButton("autoLagTolerance")
	AutoLagTolerance:SetPoint("TOPLEFT", AutoSell, "BOTTOMLEFT", 0, -8)
	AutoLagTolerance:SetText(L["Update Your Lag Threshold To Your Current MS"])

	local DeclinePvPDuel = self:CreateCheckButton("declinePvPDuel")
	DeclinePvPDuel:SetPoint("TOPLEFT", AutoLagTolerance, "BOTTOMLEFT", 0, -8)
	DeclinePvPDuel:SetText(L["Decline All Player Vs Player Duels"])

	local DeclinePetDuel = self:CreateCheckButton("declinePetDuel")
	DeclinePetDuel:SetPoint("TOPLEFT", DeclinePvPDuel, "BOTTOMLEFT", 0, -8)
	DeclinePetDuel:SetText(L["Decline All Pet Vs Pet Duels"])
end)

local Tooltip = General:CreateChild("Tooltip", "ProjectKkthnxTooltipDB", TooltipDefaults)
Tooltip:Initialize(function(self)
	local Title = self:CreateTitle()
	Title:SetPoint("TOPLEFT", 16, -16)
	Title:SetFormattedText(L["Tooltip"])

	local Description = self:CreateDescription()
	Description:SetPoint("TOPLEFT", Title, "BOTTOMLEFT", 0, -8)
	Description:SetPoint("RIGHT", -32, 0)
	Description:SetText(L["Tooltip Quality Of Life Tweaks"])

	local TooltipSpecItemLevel = self:CreateCheckButton("tooltipSpecItemLevel")
	TooltipSpecItemLevel:SetPoint("TOPLEFT", Description, "BOTTOMLEFT", 0, -8)
	TooltipSpecItemLevel:SetText(L["Show ItemLevel and Spec In Tooltip"])

	local TooltipSpecItemLevelByShift = self:CreateCheckButton("tooltipSpecItemLevelByShift")
	TooltipSpecItemLevelByShift:SetPoint("TOPLEFT", TooltipSpecItemLevel, "BOTTOMLEFT", 0, -8)
	TooltipSpecItemLevelByShift:SetText(L["Show ItemLevel and Spec In Tooltip With SHIFT Only"])

	TooltipSpecItemLevel:On("Update", "Click", function(self)
		TooltipSpecItemLevelByShift:SetEnabled(self:GetChecked())
	end)

	local TooltipIcons = self:CreateCheckButton("tooltipIcons")
	TooltipIcons:SetPoint("TOPLEFT", TooltipSpecItemLevelByShift, "BOTTOMLEFT", 0, -8)
	TooltipIcons:SetText(L["Show Icons In Tooltip For Gear, Spells And More"])

	local TooltipStyle = self:CreateCheckButton("tooltipStyle")
	TooltipStyle:SetPoint("TOPLEFT", TooltipIcons, "BOTTOMLEFT", 0, -8)
	TooltipStyle:SetText(L["Cleanup And Style The Tooltip"])
end)

local Minimap = General:CreateChild("Minimap", "ProjectKkthnxMinimapDB", MinimapDefaults)
Minimap:Initialize(function(self)
	local Title = self:CreateTitle()
	Title:SetPoint("TOPLEFT", 16, -16)
	Title:SetFormattedText(L["Minimap"])

	local Description = self:CreateDescription()
	Description:SetPoint("TOPLEFT", Title, "BOTTOMLEFT", 0, -8)
	Description:SetPoint("RIGHT", -32, 0)
	Description:SetText(L["Minimap Quality Of Life Tweaks"])

	local MinimapPerformance = self:CreateCheckButton("minimapPerformance")
	MinimapPerformance:SetPoint("TOPLEFT", Description, "BOTTOMLEFT", 0, -8)
	MinimapPerformance:SetText(L["Show Your FPS and MS Above The Minimap"])

	local MinimapCoords = self:CreateCheckButton("minimapCoords")
	MinimapCoords:SetPoint("TOPLEFT", MinimapPerformance, "BOTTOMLEFT", 0, -8)
	MinimapCoords:SetText(L["Show Your Character Coords On The Minimap"])
end)

local Chat = General:CreateChild("Chat", "ProjectKkthnxChatDB", ChatDefaults)
Chat:Initialize(function(self)
	local Title = self:CreateTitle()
	Title:SetPoint("TOPLEFT", 16, -16)
	Title:SetFormattedText(L["Chat"])

	local Description = self:CreateDescription()
	Description:SetPoint("TOPLEFT", Title, "BOTTOMLEFT", 0, -8)
	Description:SetPoint("RIGHT", -32, 0)
	Description:SetText(L["Chat Quality Of Life Tweaks"])

	local ChatUnclamp = self:CreateCheckButton("chatUnclamp")
	ChatUnclamp:SetPoint("TOPLEFT", Description, "BOTTOMLEFT", 0, -8)
	ChatUnclamp:SetText(L["Unclamp Your Chat Window So You Can Move It Lower"])

	local ChatArrowKeys = self:CreateCheckButton("chatArrowKeys")
	ChatArrowKeys:SetPoint("TOPLEFT", ChatUnclamp, "BOTTOMLEFT", 0, -8)
	ChatArrowKeys:SetText(L["Allow The Use Of The Arrow Keys In Chat"])

	local ChatHideButtons = self:CreateCheckButton("chatHideButtons")
	ChatHideButtons:SetPoint("TOPLEFT", ChatArrowKeys, "BOTTOMLEFT", 0, -8)
	ChatHideButtons:SetText(L["Hide All The Chat Buttons"])

	local ChatEditBoxToTop = self:CreateCheckButton("chatEditBoxToTop")
	ChatEditBoxToTop:SetPoint("TOPLEFT", ChatHideButtons, "BOTTOMLEFT", 0, -8)
	ChatEditBoxToTop:SetText(L["Move The Chat EditBox Ontop Of The Chat Frame"])

	local ChannelRename = self:CreateCheckButton("channelRename")
	ChannelRename:SetPoint("TOPLEFT", ChatEditBoxToTop, "BOTTOMLEFT", 0, -8)
	ChannelRename:SetText(L["Shorten Chat Channel Names. Example: 'GUILD --> G'"])

	local chatCopy = self:CreateCheckButton("chatCopy")
	chatCopy:SetPoint("TOPLEFT", ChannelRename, "BOTTOMLEFT", 0, -8)
	chatCopy:SetText(L["Add A Copy Chat Button To The ChatFrame"])
end)

local Unitframe = General:CreateChild("Unitframe", "ProjectKkthnxUnitframeDB", UnitframeDefaults)
Unitframe:Initialize(function(self)
	local Title = self:CreateTitle()
	Title:SetPoint("TOPLEFT", 16, -16)
	Title:SetFormattedText(L["Unitframe"])

	local Description = self:CreateDescription()
	Description:SetPoint("TOPLEFT", Title, "BOTTOMLEFT", 0, -8)
	Description:SetPoint("RIGHT", -32, 0)
	Description:SetText(L["Unitframe Quality Of Life Tweaks"])

	local ClassColorHealth = self:CreateCheckButton("classColorHealth")
	ClassColorHealth:SetPoint("TOPLEFT", Description, "BOTTOMLEFT", 0, -8)
	ClassColorHealth:SetText(L["Color Health Bars Based On Player Class"])

	local ClassPortraits = self:CreateCheckButton("classPortraits")
	ClassPortraits:SetPoint("TOPLEFT", ClassColorHealth, "BOTTOMLEFT", 0, -8)
	ClassPortraits:SetText(L["Show Class Icon Portaits Instead Of Default Portraits"])
end)